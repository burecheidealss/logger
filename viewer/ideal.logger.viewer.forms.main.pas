unit ideal.logger.viewer.forms.main;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  System.Generics.Collections,
  ideal.logger.viewer.stores.files,
  ideal.logger.viewer.stores.open,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.Objects,
  FMX.Layouts,
  FMX.TreeView,
  FMX.Ani,
  FMX.StdCtrls,
  FMX.Effects;

type
  TFileTreeViewItem = class(TTreeViewItem)
  private
    FFileSize : TText;
    FItem: TFilesStore.TFileRecord;
    procedure SetItem(const Value: TFilesStore.TFileRecord);
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    property Item : TFilesStore.TFileRecord read FItem write SetItem;
  end;

  TEntryTreeViewItem = class(TTreeViewItem)
  private
    FItem : TOpenStore.ILineGroupInfo;
    FDuration : TText;
    FDelta : TText;
    FLevel : TText;
    FID : TText;
    FMsg : TText;
    procedure SetItem(const Value: TOpenStore.ILineGroupInfo);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Item : TOpenStore.ILineGroupInfo read FItem write SetItem;

  end;

  TfrmMain = class(TForm)
    rectHeader: TRectangle;
    txtMenu: TText;
    tvFiles: TTreeView;
    tviApplications: TTreeViewItem;
    tviDownloads: TTreeViewItem;
    tviOther: TTreeViewItem;
    ColorAnimation1: TColorAnimation;
    txtOpen: TText;
    ColorAnimation2: TColorAnimation;
    dlgOpen: TOpenDialog;
    Splitter: TSplitter;
    tvOpenFile: TTreeView;
    seMenu: TShadowEffect;
    seOpen: TShadowEffect;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure txtMenuClick(Sender: TObject);
    procedure txtOpenClick(Sender: TObject);
    procedure tvFilesChange(Sender: TObject);
  strict private
    FFilesWidth : Single;
    procedure FillFromParent(tviParent : TTreeViewItem; Files : TList<TFilesStore.TFileRecord>);
    procedure UpdateFiles(Files : TList<TFilesStore.TFileRecord>);
    procedure UpdateAppFiles(Files : TDictionary<string, TList<TFilesStore.TFileRecord>>);
    procedure UpdateDownloads(Files : TList<TFilesStore.TFileRecord>);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  cocinasync.flux.fmx.actions.ui,
  ideal.logger.viewer.actions;

{$R *.fmx}

procedure TfrmMain.FillFromParent(tviParent: TTreeViewItem;
  Files: TList<TFilesStore.TFileRecord>);
var
  i, j: Integer;
begin
  tviParent.BeginUpdate;
  try
    i := 0;
    j := 0;
    while j < Files.Count-1 do
    begin
      if i < tviParent.Count-1 then
      begin
        tviParent.items[i].Text := Files[j].FileName;
        tviParent.items[i].TagString := Files[j].Path;
      end else
      begin
        var tvi := TFileTreeViewItem.Create(tvFiles);
        tvi.Parent := tviParent;
        tvi.Item := Files[j];
        tvi.TagString := Files[j].Path;
      end;
      inc(i);
      inc(j);
    end;
    while i < tviParent.Count do
    begin
      var tvi := tviParent.Items[i];
      tvi.Parent := nil;
      tvi.Free;
    end;
  finally
    tviParent.EndUpdate;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FFilesWidth := tvFiles.Width;
  TFilesStore.Data.RegisterForUpdates<TFilesStore>(Self,
    procedure(Store : TFilesStore)
    begin
      tvFiles.BeginUpdate;
      try
        UpdateFiles(Store.Files);
        UpdateAppFiles(Store.AppFiles);
        UpdateDownloads(Store.Downloads);
      finally
        tvFiles.EndUpdate;
      end;
    end
  );
  TOpenStore.Data.RegisterForUpdates<TOpenStore>(Self,
    procedure(Store : TOpenStore)
      procedure RenderChildren(tviParent : TTreeViewITem; Items : TList<TOpenStore.ILineGroupInfo>);
      begin
        if Assigned(tviParent) then
          tviParent.BeginUpdate;
        try
          for var item in Items do
          begin
            var tvi := TEntryTreeViewItem.Create(tvOpenFile);
            tvi.Item := item;
            if Assigned(tviParent) then
              tvi.Parent := tviParent
            else
              tvi.Parent := tvOpenFile;
            RenderChildren(tvi, item.Items);
          end;
        finally
          if Assigned(tviParent) then
            tviParent.EndUpdate;
        end;
      end;
    begin
      tvOpenFile.BeginUpdate;
      try
        while tvOpenFile.Count > 0 do
        begin
          var tvi := tvOpenFile.Items[0];
          tvi.Parent := nil;
          tvi.Free;
        end;
        RenderChildren(nil, Store.Items);
      finally
        tvOpenFile.EndUpdate;
      end;
    end
  );
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  TFilesStore.Data.UnregisterForUpdates(Self);
  TOpenStore.Data.UnregisterForUpdates(Self);
end;

procedure TfrmMain.tvFilesChange(Sender: TObject);
begin
  if tvFiles.Selected <> nil then
  begin
    if tvFiles.Selected.TagString <> '' then
      TViewLogFileAction.Post(tvFiles.Selected.TagString);
  end;
end;

procedure TfrmMain.txtMenuClick(Sender: TObject);
begin
  if tvFiles.Width = 0 then
  begin
    TAnimator.AnimateFloat(tvFiles, 'Width', FFilesWidth);
    Splitter.Visible := True;
    seMenu.Enabled := False;
  end else
  begin
    FFilesWidth := tvFiles.Width;
    TAnimator.AnimateFloat(tvFiles, 'Width', 0);
    Splitter.Visible := False;
    seMenu.Enabled := True;
  end;
end;

procedure TfrmMain.txtOpenClick(Sender: TObject);
begin
  if dlgOpen.Execute then
  begin
    for var sFile in dlgOpen.Files do
    begin
      TOpenLogFileAction.Post(sFile);
    end;
  end;
end;

procedure TfrmMain.UpdateAppFiles(Files: TDictionary<string, TList<TFilesStore.TFileRecord>>);
var
  bFound : boolean;
begin
  var ary := Files.ToArray;
  for var a in ary do
  begin
    bFound := False;
    for var i := 0 to tviApplications.Count-1 do
    begin
      if tviApplications.Items[i].Text = a.Key then
      begin
        bFound := True;
        FillFromParent(tviApplications.Items[i], a.Value);
        break;
      end;
    end;
    if not bFound then
    begin
      var tvi := TTreeViewItem.Create(tvFiles);
      tvi.Parent := tviApplications;
      tvi.Text := a.Key;
      FillFromParent(tvi, a.Value);
    end;
  end;
end;

procedure TfrmMain.UpdateDownloads(Files: TList<TFilesStore.TFileRecord>);
  function FindParent(Text : string) : TTreeViewItem;
  var
    i : integer;
  begin
    Result := nil;
    for i := 0 to tviDownloads.Count-1 do
    begin
      if tviDownloads.Items[i].Text = Text then
      begin
        Result := tviDownloads.Items[i];
        break;
      end;
    end;
    if not Assigned(Result) then
    begin
      Result := TTreeViewItem.Create(tvFiles);
      Result.Text := Text;
      Result.Parent := tviDownloads;
    end;
  end;
  procedure AddFile(Parent : TTreeViewItem; FileInfo : TFilesStore.TFileRecord);
  begin
    var tvi := TFileTreeViewItem.Create(tvFiles);
    tvi.Item := FileInfo;
    tvi.TagString := FileInfo.Filepath;
    tvi.Parent := Parent;
  end;
begin
  tviDownloads.BeginUpdate;
  try
    while tviDownloads.Count > 0 do
    begin
      var tvi := tviDownloads.Items[0];
      tvi.Parent := nil;
      tvi.Free;
    end;
    for var fi in Files do
    begin
      if fi.RelativeFilePath = fi.FileName then
        AddFile(tviDownloads, fi)
      else
        AddFile(FindParent(fi.RelativeFilePath.Replace(fi.FileName,'')), fi);
    end;
  finally
    tviDownloads.EndUpdate;
  end;
end;

procedure TfrmMain.UpdateFiles(Files: TList<TFilesStore.TFileRecord>);
begin
  FillFromParent(tviOther, Files);
end;

{ TEntryTreeViewItem }

constructor TEntryTreeViewItem.Create(AOwner: TComponent);
begin
  inherited;
  FItem := nil;
  FDuration := TText.Create(Self);
  FDelta := TText.Create(Self);
  FLevel := TText.Create(Self);
  FID := TText.Create(Self);
  FMsg := TText.Create(Self);

  BeginUpdate;
  try
    FID.Parent := Self;
    FID.TextSettings.HorzAlign := TextAlign.taLeading;
    FID.TextSettings.VertAlign := TTextAlign.Leading;
    FID.Align := TAlignLayout.Left;
    FID.Width := 75;
    FID.Margins.Left := 30;
    FID.HitTest := False;
    FID.Locked := True;

    FDelta.Parent := Self;
    FDelta.Align := TAlignLayout.Left;
    FDelta.TextSettings.VertAlign := TTextAlign.Leading;
    FDelta.TextSettings.HorzAlign := TextAlign.taLeading;
    FDelta.Width := 75;
    FDelta.HitTest := False;
    FDelta.Locked := True;

    FDuration.Parent := Self;
    FDuration.Align := TAlignLayout.Left;
    FDuration.TextSettings.VertAlign := TTextAlign.Leading;
    FDuration.TextSettings.HorzAlign := TextAlign.taLeading;
    FDuration.Width := 75;
    FDuration.HitTest := False;
    FDuration.Locked := True;

    FLevel.Parent := Self;
    FLevel.Align := TAlignLayout.Right;
    FLevel.TextSettings.VertAlign := TTextAlign.Leading;
    FLevel.TextSettings.HorzAlign := TextAlign.taLeading;
    FLevel.Width := 75;
    FLevel.HitTest := False;
    FLevel.Locked := True;

    FMsg.Parent := Self;
    FMsg.Align := TAlignLayout.Client;
    FMsg.TextSettings.WordWrap := False;
    FMsg.TextSettings.HorzAlign := TextAlign.taLeading;
    FMsg.TextSettings.VertAlign := TTextAlign.Leading;
    FMsg.HitTest := False;
    FMsg.Locked := True;
  finally
    EndUpdate;
  end;
end;

destructor TEntryTreeViewItem.Destroy;
begin
  FDuration.Free;
  FDelta.Free;
  FLevel.Free;
  FID.Free;
  FMsg.Free;
  inherited;
end;

procedure TEntryTreeViewItem.SetItem(const Value: TOpenStore.ILineGroupInfo);
begin
  FItem := Value;
  FDuration.Text := FItem.Duration.ToString;
  FDelta.Text := FItem.Delta.ToString;
  FLevel.Text := DateToStr(FItem.Timestamp);
  FID.Text := FItem.LineFrom.ToString;
  FMsg.Text := FItem.Text;
  Hint := FItem.Text;
  ShowHint := True;
  FLevel.Text := FItem.Level;
end;

{ TFileTreeViewItem }

constructor TFileTreeViewItem.Create(AOwner: TComponent);
begin
  inherited;
  FFileSize := TText.Create(Self);
end;

destructor TFileTreeViewItem.Destroy;
begin

  inherited;
end;

procedure TFileTreeViewItem.SetItem(const Value: TFilesStore.TFileRecord);
  function FileSizeToText(Size : UInt64) : string;
  begin
    if Size < 1024 then
      Result := Size.ToString+' Bytes'
    else if Size < (1024*100) then
      Result := (Round((Size / 1024) * 100) / 100).ToString+' KB'
    else if Size < (1024*10000) then
      Result := (Round((Size / 10240) * 100) / 100).ToString+' MB'
    else
      Result := (Round((Size / 10240) * 1000) / 100).ToString+' GB';
  end;
begin
  FItem := Value;
  FFileSize.Parent := Self;
  FFileSize.Align := TAlignLayout.Right;
  FFileSize.Text := FileSizeToText(Item.Size);
  FFileSize.Width := 75;
  FFileSize.HitTest := False;
  FFileSize.Locked := False;
  Text := Value.FileName;
end;

end.
