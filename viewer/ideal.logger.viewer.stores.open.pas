unit ideal.logger.viewer.stores.open;

interface

uses
  System.Classes,
  System.SysUtils,
  System.Generics.Collections,
  System.UITypes,
  cocinasync.flux.store,
  cocinasync.Async,
  ideal.logger.viewer.actions;

type
  TOpenStore = class(TBaseStore)
  public
    type
      TLineInfo = record
        LineNumber : UInt64;
        Msg : String;
        Thread : Int64;
        Level : String;
        Delta : Int64;
        Duration : Double;
        StartPOS : Integer;
        Length : Integer;
        TimeStamp : TDateTime;
        class function From(AStream : TStream; AStartPOS, ALength : Integer) : TLineInfo; static;
      end;

      ILineGroupInfo = interface
        function GetLineFrom : UInt64;
        function GetLineTo : UInt64;
        function GetDuration : Double;
        function GetText : string;
        function GetNotes : string;
        function GetColor : TAlphaColor;
        function GetDelta : Int64;
        function GetItems : TList<ILineGroupInfo>;
        function GetTimestamp : TDateTime;
        function GetThread : Int64;
        function GetLevel : String;

        property LineFrom : UInt64 read GetLineFrom;
        property LineTo : UInt64 read GetLineTo;
        property Duration : Double read GetDuration;
        property Notes : string read GetNotes;
        property Text : string read GetText;
        property Items : TList<ILineGroupInfo> read GetItems;
        property Delta : Int64 read GetDelta;
        property Timestamp : TDateTime read GetTimestamp;
        property Color : TAlphaColor read GetColor;
        property Thread : Int64 read GetThread;
        property Level : String read GetLevel;
      end;

      TLineGroupInfo = class(TInterfacedObject, ILineGroupInfo)
      private
        FLineFrom : UInt64;
        FLineTo : UInt64;
        FDuration : Double;
        FText : string;
        FNotes : string;
        FItems : TList<ILineGroupInfo>;
        FDelta : Int64;
        FTimestamp : TDateTime;
        FColor : TAlphaColor;
        FThread : Int64;
        FLevel : String;

        function GetLineFrom : UInt64;
        function GetLineTo : UInt64;
        function GetDuration : Double;
        function GetText : string;
        function GetNotes : string;
        function GetDelta : Int64;
        function GetItems : TList<ILineGroupInfo>;
        function GetTimestamp : TDateTime;
        function GetColor : TAlphaColor;
        function GetThread : Int64;
        function GetLevel : String;
      public
        property LineFrom : UInt64 read GetLineFrom;
        property LineTo : UInt64 read GetLineTo;
        property Duration : Double read GetDuration;
        property Notes : string read GetNotes;
        property Delta : Int64 read GetDelta;
        property Items : TList<ILineGroupInfo> read GetItems;
        property Text : string read GetText;
        property Timestamp : TDateTime read GetTimestamp;
        property Color : TAlphaColor read GetColor;
        property Thread : Int64 read GetThread;
        property Level : String read GetLevel;

        constructor Create(Line : TLineInfo); reintroduce;
        destructor Destroy; override;
      end;
  strict private
    class var FData : TOpenStore;
  strict private
    FFile : TFileStream;
    FFilePath : string;
    FLines : TList<TLineInfo>;
    FItems : TList<ILineGroupInfo>;
    procedure OnViewLogFile(Sender : TObject; Action : TViewLogFileAction);
    procedure ProcessLineInfo;
    procedure ApplyFilters;
  public
    constructor Create; override;
    destructor Destroy; override;

    class property Data : TOpenStore read FData;
    class constructor Create;
    class destructor Destroy;

    property FilePath : String read FFilePath;
    property Lines : TList<TLineInfo> read FLines;
    property Items : TList<ILineGroupInfo> read FItems;
  end;

implementation

uses
  System.IOUtils,
  chimera.json,
  cocinasync.jobs,
  cocinasync.flux.dispatcher,
  cocinasync.flux.fmx.actions.ui;

{ TOpenStore }

constructor TOpenStore.Create;
begin
  inherited;
  FFile := nil;
  FLines := TList<TLineInfo>.Create;
  FItems := TList<ILineGroupInfo>.Create;
  Flux.Register<TViewLogFileAction>(Self, OnViewLogFile);
end;

procedure TOpenStore.ApplyFilters;
var
  ThreadStacks : TDictionary<Int64, TStack<TLineGroupInfo>>;
  Stack : TStack<TLineGroupInfo>;
  idx : integer;
  lgi : TLineGroupInfo;
  a: TPair<Int64, TStack<TLineGroupInfo>>;
begin
  FItems.Clear;
  idx := 0;
  ThreadStacks := TDictionary<Int64, TStack<TLineGroupInfo>>.Create;
  try
    repeat
      if not ThreadStacks.ContainsKey(FLines[idx].Thread) then
      begin
        Stack := TStack<TLineGroupInfo>.Create;
        ThreadStacks.Add(FLines[idx].Thread, Stack);
      end else
        Stack := ThreadStacks[FLines[idx].Thread];

      if FLines[idx].Level = 'ENTER' then
      begin
        lgi := TLineGroupInfo.Create(FLines[idx]);
        Stack.Push(lgi);
        FItems.Add(lgi)
      end else if FLines[idx].Level = 'EXIT' then
      begin
        if (Stack.Count > 0) then
        begin
          if Stack.Peek.Text = FLines[idx].Msg then
          begin
            lgi := Stack.Pop;
            lgi.FDuration := FLines[idx].Duration;
          end else
          begin
            var StackWalker := TStack<TLineGroupInfo>.Create;
            repeat
              StackWalker.Push(Stack.Pop);
            until (Stack.Count = 0) or (Stack.Peek.Text = FLines[idx].Msg);
            if Stack.Count > 0 then
            begin
              lgi := Stack.Pop;
              lgi.FDuration := FLines[idx].Duration;
            end;
            while StackWalker.Count > 0 do
              Stack.Push(StackWalker.Pop);
          end;
        end;
      end else
      begin
        lgi := TLineGroupInfo.Create(FLines[idx]);

        if Stack.Count > 0 then
          Stack.Peek.Items.Add(lgi)
        else
          FItems.Add(lgi);
      end;
      inc(idx);
    until idx >= FLines.Count;
  finally
    for a in ThreadStacks.ToArray do
    begin
      a.Value.Free;
    end;
    ThreadStacks.Free;
  end;
end;

class constructor TOpenStore.Create;
begin
  FData := TOpenStore.Create;
end;

destructor TOpenStore.Destroy;
begin
  FFile.Free;
  FLines.Free;
  FItems.Free;
  inherited;
end;

class destructor TOpenStore.Destroy;
begin
  FData.Free;
end;

procedure TOpenStore.OnViewLogFile(Sender: TObject; Action: TViewLogFileAction);
begin
  if Assigned(FFile) then
    FFile.Free;
  FFilepath := Action.FilePath;
  FFile := TFileStream.Create(FFilepath, fmOpenRead or fmShareDenyNone);
  ProcessLineInfo();
  ApplyFilters();
end;

procedure TOpenStore.ProcessLineInfo;
const
  BUFF_SIZE = 4096;
var
  buff : TArray<Byte>;
  iBuffLength : integer;
  iFSBufferStart : integer;
  iLastLinePos : integer;
  iBuffEOLIdx : integer;
  i: integer;
begin
  iFSBufferStart := 0;
  iBuffEOLIdx := 0;
  iLastLinePos := 0;
  FFile.Position := 0;
  SetLength(buff, BUFF_SIZE);
  repeat
    iFSBufferStart := FFile.Position;
    iBuffLength := FFile.Read(buff, BUFF_SIZE);
    if iBuffLength > 0 then
    begin
      for i := 0 to iBuffLength-1 do
      begin
        if (buff[i] = 13) or (buff[i] = 10) then
        begin
          iBuffEOLIdx := i;
          if iBuffEOLIdx+iFSBufferStart-iLastLinePos > 1 then
            FLines.Add(TOpenStore.TLineInfo.From(FFile, iLastLinePos, (iBuffEOLIdx+iFSBufferStart)-iLastLinePos));
          iLastLinePos := iFSBufferStart+i+1;
        end;
      end;
    end;
  until FFile.Position >= FFile.Size;
end;

{ TOpenStore.TLineInfo }

class function TOpenStore.TLineInfo.From(AStream : TStream; AStartPOS, ALength: Integer): TLineInfo;
var
  ary : TArray<byte>;
begin
  Result.StartPOS := AStartPOS;
  Result.Length := ALength;
  var iPos := AStream.Position;
  SetLength(ary, ALength);

  AStream.Position := AStartPOS;
  AStream.Read(ary, ALength);
  AStream.Position := iPos;

  var jso := TJSON.From(TEncoding.UTF8.GetString(ary));
  Result.LineNumber := jso.Integers['@i'];
  Result.Msg := jso.Strings['@m'];
  Result.Level := jso.Strings['@l'];
  Result.Delta := jso.Integers['@ms_delta'];
  Result.Thread := jso.Integers['@thread_id'];
  Result.TimeStamp := jso.Dates['@t'];
  if jso.Has['@method_duration'] and (jso.Types['@method_duration'] = TJSONValueType.number) then
    Result.Duration := jso.Numbers['@method_duration']
  else
    Result.Duration := 0;
end;

{ TOpenStore.TLineGroupInfo }

constructor TOpenStore.TLineGroupInfo.Create(Line : TLineInfo);
begin
  inherited Create;
  FItems := TList<ILineGroupInfo>.Create;
  FLineFrom := Line.LineNumber;
  FLineTo := Line.LineNumber;
  FDuration := Line.Duration;
  FDelta := Line.Delta;
  FText := Line.Msg;
  FNotes := '';
  FThread := Line.Thread;
  FLevel := Line.Level;
  if Line.Level = 'DEBUG' then
    FColor := TAlphaColorRec.Azure
  else if Line.Level = 'TRACE' then
    FColor := TAlphaColorRec.Beige
  else if Line.Level = 'INFO' then
    FColor := TAlphaColorRec.Burlywood
  else if Line.Level = 'ERROR' then
    FColor := TAlphaColorRec.Red
  else if Line.Level = 'WARNING' then
    FColor := TAlphaColorRec.Orange
  else
    FColor := TAlphaColorRec.White;
end;

destructor TOpenStore.TLineGroupInfo.Destroy;
begin
  FItems.Free;
  inherited;
end;

function TOpenStore.TLineGroupInfo.GetColor: TAlphaColor;
begin
  Result := FColor;
end;

function TOpenStore.TLineGroupInfo.GetDelta: Int64;
begin
  Result := FDelta;
end;

function TOpenStore.TLineGroupInfo.GetDuration: Double;
begin
  Result := FDuration;
end;

function TOpenStore.TLineGroupInfo.GetItems: TList<ILineGroupInfo>;
begin
  Result := FItems;
end;

function TOpenStore.TLineGroupInfo.GetLevel: String;
begin
  Result := FLevel;
end;

function TOpenStore.TLineGroupInfo.GetLineFrom: UInt64;
begin
  Result := FLineFrom;
end;

function TOpenStore.TLineGroupInfo.GetLineTo: UInt64;
begin
  Result := FLineTo;
end;

function TOpenStore.TLineGroupInfo.GetNotes: string;
begin
  Result := FNotes;
end;

function TOpenStore.TLineGroupInfo.GetText: string;
begin
  Result := FText;
end;

function TOpenStore.TLineGroupInfo.GetThread: Int64;
begin
  Result := FThread;
end;

function TOpenStore.TLineGroupInfo.GetTimestamp: TDateTime;
begin
  Result := FTimestamp;
end;

end.
