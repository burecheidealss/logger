unit ideal.logger.viewer.actions;

interface

uses
  System.Classes,
  System.SysUtils,
  cocinasync.flux.action;

type
  TViewLogFileAction = class(TBaseAction)
  private
    FFilePath : string;
  public
    class procedure Post(const AFilePath : string);
    property FilePath : string read FFilePath;
  end;

  TCompareLogFilesAction = class(TBaseAction)
  private
    FFile1 : string;
    FFile2 : string;
  public
    class procedure Post(const AFile1, AFile2 : string);
    property File1: string read FFile1;
    property File2: string read FFile2;
  end;

  TOpenLogFileAction = class(TBaseAction)
  private
    FFilePath : string;
  public
    class procedure Post(const AFilePath : string);
    property FilePath : string read FFilePath;
  end;

  TCloseLogFileAction = class(TBaseAction)
  private
    FFilePath : string;
  public
    class procedure Post(const AFilePath : string);
    property FilePath : string read FFilePath;
  end;

  TDeleteLogFileAction = class(TBaseAction)
  private
    FFilePath : string;
  public
    class procedure Post(const AFilePath : string);
    property FilePath : string read FFilePath;
  end;

implementation

{ TSelectLogFileAction }

class procedure TOpenLogFileAction.Post(const AFilePath: string);
begin
  var act := inherited New<TOpenLogFileAction>;
  act.FFilePath := AFilePath;
  act.Dispatch;
end;

{ TCloseLogFileAction }

class procedure TCloseLogFileAction.Post(const AFilePath: string);
begin
  var act := inherited New<TCloseLogFileAction>;
  act.FFilePath := AFilePath;
  act.Dispatch;
end;

{ TDeleteLogFileAction }

class procedure TDeleteLogFileAction.Post(const AFilePath: string);
begin
  var act := inherited New<TDeleteLogFileAction>;
  act.FFilePath := AFilePath;
  act.Dispatch;
end;

{ TCompareLogFilesAction }

class procedure TCompareLogFilesAction.Post(const AFile1, AFile2: string);
begin
  var act := inherited New<TCompareLogFilesAction>;
  act.FFile1 := AFile1;
  act.FFile2 := AFile2;
  act.Dispatch;
end;

{ TViewLogFileAction }

class procedure TViewLogFileAction.Post(const AFilePath: string);
begin
  var act := inherited New<TViewLogFileAction>;
  act.FFilePath := AFilePath;
  act.Dispatch;
end;

end.
