program ideal.logger.viewer;

uses
  System.StartUpCopy,
  FMX.Forms,
  ideal.logger.viewer.forms.main in 'ideal.logger.viewer.forms.main.pas' {frmMain},
  ideal.logger.viewer.stores.files in 'ideal.logger.viewer.stores.files.pas',
  ideal.logger.viewer.actions in 'ideal.logger.viewer.actions.pas',
  ideal.logger.viewer.stores.open in 'ideal.logger.viewer.stores.open.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
