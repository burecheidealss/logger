unit ideal.logger.viewer.stores.files;

interface

uses
  System.Classes,
  System.SysUtils,
  System.Generics.Collections,
  cocinasync.flux.store,
  cocinasync.Async,
  ideal.logger.viewer.actions;

type
  TFilesStore = class(TBaseStore)
  public
    type
      TFileRecord = record
        FileName : string;
        Path : string;
        Root : string;
        Size : UInt64;
        class function From(const AFilepath, ARoot : string) : TFileRecord; static;
        function FilePath : string;
        function RelativeFilePath : string;
      end;
  strict private
    class var FData : TFilesStore;
  strict private
    FAppFiles: TDictionary<string, TList<TFileRecord>>;
    FFiles: TList<TFileRecord>;
    FDownloads : TList<TFileRecord>;
    FPersistFilename : string;
    FAsync : TAsync;
    function IsValidFilename(Filename : string) : boolean;
    procedure RestoreFileList;
    procedure PersistFileList;
    procedure OnSelectedFile(Sender : TObject; Action : TOpenLogFileAction);
    procedure OnCloseFile(Sender : TObject; Action : TCloseLogFileAction);
    procedure OnDeleteFile(Sender : TObject; Action : TDeleteLogFileAction);
    procedure StartFileMonitor;
  public
    constructor Create; override;
    destructor Destroy; override;

    class property Data : TFilesStore read FData;
    class constructor Create;
    class destructor Destroy;


    property Files : TList<TFileRecord> read FFiles;
    property AppFiles : TDictionary<string, TList<TFileRecord>> read FAppFiles;
    property Downloads : TList<TFileRecord> read FDownloads;
  end;

implementation

uses
  System.IOUtils,
  chimera.json,
  cocinasync.jobs,
  cocinasync.flux.dispatcher;

{ TFilesStore }

class constructor TFilesStore.Create;
begin
  FData := TFilesStore.Create;
end;

destructor TFilesStore.Destroy;
begin
  FAsync.Free;
  FFiles.Free;
  FDownloads.Free;
  FAppFiles.Free;

  Flux.Unregister<TOpenLogFileAction>(Self);
  Flux.Unregister<TCloseLogFileAction>(Self);
  Flux.Unregister<TDeleteLogFileAction>(Self);
  inherited;
end;

constructor TFilesStore.Create;
begin
  inherited;
  FAsync := TAsync.Create;
  FAppFiles := TDictionary<string, TList<TFileRecord>>.Create;
  FFiles := TList<TFileRecord>.Create;
  FDownloads := TList<TFileRecord>.Create;
  FPersistFilename := TPath.Combine(TPath.GetCachePath, 'ideal.logger.vieweer.json');

  Flux.Register<TOpenLogFileAction>(Self,OnSelectedFile);
  Flux.Register<TCloseLogFileAction>(Self, OnCloseFile);
  Flux.Register<TDeleteLogFileAction>(Self, OnDeleteFile);

  RestoreFileList;

  StartFileMonitor;
end;

class destructor TFilesStore.Destroy;
begin
  FData.Free;
end;

function TFilesStore.IsValidFilename(Filename: string): boolean;
var
  ary : TArray<String>;
begin
  Result := Filename.EndsWith('.json') or Filename.EndsWith('.jsonz');
  if Result then
  begin
    ary := Filename.Split(['.']);
    Result := Length(ary) >= 6;
    if Result then
    begin
      Result := (Length(ary[Length(ary)-5]) = 13) and
                (Length(ary[Length(ary)-4]) = 2) and
                (Length(ary[Length(ary)-3]) = 2) and
                (Length(ary[Length(ary)-2]) = 3);

    end;

  end;
end;

procedure TFilesStore.OnCloseFile(Sender: TObject;
  Action: TCloseLogFileAction);
begin
  for var i := FFiles.Count-1 downto 0 do
  begin
    if TPath.Combine(FFiles[1].Path, FFiles[i].FileName) = Action.FilePath then
      FFiles.Delete(i);
  end;
  PersistFileList;
end;

procedure TFilesStore.OnDeleteFile(Sender: TObject;
  Action: TDeleteLogFileAction);
begin
  for var i := FFiles.Count-1 downto 0 do
  begin
    if TPath.Combine(FFiles[1].Path, FFiles[i].FileName) = Action.FilePath then
      FFiles.Delete(i);
  end;
  TFile.Delete(Action.FilePath);
  PersistFileList;
end;

procedure TFilesStore.OnSelectedFile(Sender: TObject;
  Action: TOpenLogFileAction);
begin
  FFiles.Add(TFilesStore.TFileRecord.From(Action.Filepath, ''));
  PersistFileList;
end;

procedure TFilesStore.PersistFileList;
var
  jso : IJSONObject;
  jsa : IJSONArray;
  i : integer;
begin
  jsa := TJSONArray.New;
  for i := 0 to FFiles.Count-1 do
    jsa.Add(FFiles[i].FilePath);
  if TFile.Exists(FPersistFilename) then
    jso := TJSON.FromFile(FPersistFilename)
  else
    jso := TJSON.New;
  jso.Arrays['files'] := jsa;
  jso.SaveToFile(FPersistFilename);
end;

procedure TFilesStore.RestoreFileList;
var
  jso : IJSONObject;
begin
  FFiles.Clear;
  if TFile.Exists(FPersistFilename) then
  begin
    jso := TJSON.FromFile(FPersistFilename);
    jso.Arrays['files'].Each(
      procedure(const filename : string)
      begin
        FFiles.Add(TFilesStore.TFileRecord.From(filename, ''));
      end
    );
    UpdateViews;
  end;
end;

procedure TFilesStore.StartFileMonitor;
var
  proc : TProc;
begin
  proc := procedure
    function AddDownloadsFrom(Path : String) : boolean;
    var
      ary : TArray<string>;
      sFile : string;
    begin
      Result := False;
      if TDirectory.Exists(Path) then
      begin
        ary := TDirectory.GetFiles(Path, '*.json*', TSearchOption.soAllDirectories);
        for sFile in ary do
        begin
          var fi := TFileRecord.From(sFile, Path);
          if IsValidFilename(fi.FileName) then
            if FDownloads.IndexOf(fi) < 0 then
            begin
              FDownloads.Add(fi);
              Result := True;
            end;
        end;
      end;
    end;
    var
      ary : TArray<String>;
      sPath: string;
      sRoot : string;
      sFile: string;
      bChanged : boolean;
    begin
      bChanged := False;
      sRoot :=
        TPath.GetHomePath+TPath.DirectorySeparatorChar+
        'ideal'+TPath.DirectorySeparatorChar+
        'logs'+TPath.DirectorySeparatorChar;
      ary := TDirectory.GetDirectories(sRoot);
      for sPath in ary do
      begin
        var sFolder := sPath.Replace(sRoot,'');
        if not FAppFiles.ContainsKey(sFolder) then
        begin
          FAppFiles.Add(sFolder, TList<TFileRecord>.Create);
          bChanged := True;
        end;
        ary := TDirectory.GetFiles(sPath, '*.json*');
        for sFile in ary do
        begin
          var fi := TFileRecord.From(sFile, sRoot);
          if isValidFilename(fi.Filename) then
            if FAppFiles[sFolder].IndexOf(fi) < 0 then
            begin
              FAppFiles[sFolder].Add(fi);
              bChanged := True;
            end;
        end;
      end;


      bChanged := AddDownloadsFrom('\\Mac\\Home\Downloads\') or bChanged;
      bChanged := AddDownloadsFrom(TPath.GetDownloadsPath) or bChanged;
      bChanged := AddDownloadsFrom(TPath.GetSharedDownloadsPath) or bChanged;

      if bChanged then
        TAsync.SynchronizeIfInThread(
          procedure
          begin
            UpdateViews;
          end
        );
    end;

  proc();
  FAsync.DoEvery(
  10000,
    proc,
    False
  );
end;

{ TFilesStore.TFileRecord }

function TFilesStore.TFileRecord.FilePath: string;
begin
  Result := TPath.Combine(Path, Filename);
end;

class function TFilesStore.TFileRecord.From(
  const AFilepath, ARoot: string): TFileRecord;
begin
  Result.FileName := ExtractFilename(AFilepath);
  Result.Path := ExtractFilepath(AFilepath);
  Result.Root := ARoot;
  var fs := TFile.OpenRead(AFilePath);
  try
    Result.Size := fs.Size;
  finally
    fs.Free;
  end;

end;

function TFilesStore.TFileRecord.RelativeFilePath: string;
begin
  Result := FilePath.Replace(Root,'');
end;

end.
